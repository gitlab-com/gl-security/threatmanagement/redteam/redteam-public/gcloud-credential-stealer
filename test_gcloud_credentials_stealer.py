import os
import gcloud_credential_stealer


def test_path_exists():
    existing_path = os.path.expanduser('~')
    assert gcloud_credential_stealer.path_exists(existing_path) is True


def test_path_does_not_exist():
    non_existing_path = os.path.expanduser('~') + '/non_existing_path'
    assert gcloud_credential_stealer.path_exists(non_existing_path) is False


def test_gcloud_cred_databases_exist_for_happy_path():
    path = os.path.join(os.path.curdir, 'test_data', 'happy_path')
    assert gcloud_credential_stealer.gcloud_cred_databases_exist(path) is True


def test_gcloud_access_tokens_db_missing():
    path = os.path.join(os.path.curdir, 'test_data', 'missing_access_tokens_db')
    assert gcloud_credential_stealer.gcloud_cred_databases_exist(path) is False


def test_gcloud_credentials_db_missing():
    path = os.path.join(os.path.curdir, 'test_data', 'missing_credentials_db')
    assert gcloud_credential_stealer.gcloud_cred_databases_exist(path) is False


def test_zero_records_handled():
    assert gcloud_credential_stealer.dump_access_tokens_db([]) is False
    assert gcloud_credential_stealer.dump_credentials_db([]) is False


