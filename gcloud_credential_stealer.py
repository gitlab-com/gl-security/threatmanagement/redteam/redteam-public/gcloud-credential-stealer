import os
import argparse
import sqlite3


def path_exists(path):
    if os.path.exists(path):
        return True
    else:
        return False


def get_rows(full_file_path, table_name):
    conn = sqlite3.connect(full_file_path)
    cursor = conn.execute("SELECT * FROM {}".format(table_name))
    return cursor.fetchall()


def gcloud_cred_databases_exist(path):
    print("")
    if path_exists(os.path.join(path, "credentials.db")):
        print("[+] Found gcloud credentials.db")
    else:
        return False

    if path_exists(os.path.join(path, "access_tokens.db")):
        print("[+] Found gcloud access_tokens.db")
    else:
        return False
    return True


def print_header(title):
    print("")
    print("########################################")
    print(title)
    print("")


def dump_credentials_db(rows):
    print_header("Credentials")
    if len(rows) == 0:
        print("[-] No credentials found in credentials.db")
        return False
    for row in rows:
        print("{}".format(row[0]))
        print("{}".format(row[1]))
    print("")
    return True


def dump_access_tokens_db(rows):
    print_header("Access Tokens")
    if len(rows) == 0:
        print("[-] No access tokens found in access_tokens.db")
        return False
    for row in rows:
        print("AccountID: {}".format(row[0]))
        print("Access Token: {}".format(row[1]))
        print("Expires: {}".format(row[2]))
        print("RAPT: {}".format(row[3]))
    print("")
    return True


def configure_argparser():
    parser = argparse.ArgumentParser(description="Extract gcloud credentials from a gcloud credential directory of "
                                                 "your choice.  By default, gcloud credential dbs are located at "
                                                 "`~/.config/gcloud` on linux-based systems, and "
                                                 "`\\AppData\\Roaming\\gcloud\\` on Windows.  You can specify a "
                                                 "different path with the -p flag.")
    parser.add_argument("-p", "--path", help="Path to gcloud credentials", required=True)
    args = parser.parse_args()
    return args


def main():
    args = configure_argparser()
    if not gcloud_cred_databases_exist(args.path):
        print("[!] gcloud credential databases not found at {}!  Exiting...".format(args.path))
        exit(1)
    dump_credentials_db(get_rows(os.path.join(args.path, "credentials.db"), "credentials"))
    dump_access_tokens_db(get_rows(os.path.join(args.path, "access_tokens.db"), "access_tokens"))


if __name__ == "__main__":
    main()
