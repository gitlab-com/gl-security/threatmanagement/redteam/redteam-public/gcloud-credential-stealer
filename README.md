# gcloud-credential-stealer

Steals credentials from an installation of the gcloud CLI.

## Usage

```
usage: gcloud_credential_stealer.py [-h] -p PATH

Extract gcloud credentials from a gcloud credential directory of your choice. By default, gcloud credential dbs are located at `~/.config/gcloud` on linux-based systems, and
`\AppData\Roaming\gcloud\` on Windows. You can specify a different path with the -p flag.

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH  Path to gcloud credentials
```